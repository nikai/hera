@echo off
@echo 输入MySQL连接相关信息，在jsds_script.sql,jsds_init_data.sql文件修改数据库信息，默认数据库名称为JSDS.
set /p hostname=Hostname:
set /p port=Port:
set /p userName=Username:
set /p password=Password:

@echo %hostname%
@echo %port%
@echo %userName%


@echo 正在初始化数据库脚本...

mysql -h%hostname% -P%port% -u%userName% -p%password% --default-character-set=utf8  <init.sql



@echo 初始化数据库完成...

pause